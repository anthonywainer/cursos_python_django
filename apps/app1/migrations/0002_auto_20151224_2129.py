# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2015-12-25 02:29
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('app1', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='cursos',
            name='idescuela',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app1.escuelaprofesional'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='cursos',
            name='idfacultad',
            field=models.ForeignKey(default=1, on_delete=django.db.models.deletion.CASCADE, to='app1.facultades'),
            preserve_default=False,
        ),
    ]
