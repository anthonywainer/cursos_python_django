from django.contrib import admin
from .models import *

# Register your models here.
class curso(admin.ModelAdmin):
    list_display  = ('id','descripcioncurso','creditos','tipocurso', 'ciclo', 'tipocurso')
    search_fields = ('id','descripcioncurso','creditos', 'ciclo', 'tipocurso')
    list_filter   = ('idplan','tipocurso')
    fields        = ('descripcioncurso','creditos','tipocurso','ciclo','idplan')
    ordering      = ('id',)
    raw_id_fields = ('idplan',)

class facultad(admin.ModelAdmin): 
    # es para definir los campos en específico
    list_display = ('id','descripcionfacultad','estadofacultad') 

class escuela(admin.ModelAdmin):
    list_display = ('descripcionescuela','estadoescuela')

admin.site.register(cursos, curso)
admin.site.register(facultades)
admin.site.register(escuelaprofesional, escuela)
admin.site.register(plancurricular)

admin.site.register(requisitos)